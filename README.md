# docker-inventorious
This is a docker image builder that combines the back end of Inventorious-sqlite3-backend with the staticly built frontend of Inventorious.

For more information about Inventorious itself, check out 
![Inventorious on gitlab](https://gitlab.com/SuperRoach/inventorious/tree/master/search)

This requires a clone of these two repos which for reference are in:
## Front end
git clone http://gitlab.com/SuperRoach/inventorious.git
## Back end
git clone http://gitlab.com/SuperRoach/inventorious-sqlite3-backend.git
(Feel free to swap with the SSH versions if it's more conveinent for you)

# Getting started
You will need to grab the submodules / other repos first. Do that with

`git submodule update --init --force --remote`

Which will work in the root folder, grabbing all of the git repos needed and get them ready locally.

Later on if you want to grab new updates, you can go in the folder (ie inventorious) and run `git fetch` and then `git merge origin/master`. But an easier way is that you can run git `submodule update --remote` in the root (ie DockerFile) directory.

Once you have the required git repos ready, you can develop in them, or just run the dockerfile after building it:
## By using docker-compose.yml , with a persistent volume 
`docker-compose up`

## Docker build
`docker build -t yourtag:latest .`
`docker run -d -p 4500:4500 yourtag:latest`

## Setting an api address
You can tell the Front end to look for the API server in a different place, by setting
`docker build --build-arg HOST_ADDRESS=youripaddress`

## Using a volume
If you would like to preserve your database between your container stopping and starting, you can do this with the following:
Create a volume: `docker volume create db-volume`
Run your built docker image: `docker run --mount source=db-volume,target=/app/data --name db -d -p 4500:4500 yourtag:latest`
If you are copying from the root of inventorious-docker , There is a sample database there ready for testing.
`docker cp ./data/ db:/usr/src/app/`
Doing it with the docker-compose?
`docker cp ./data/ inventorious-docker_inventorious_1:usr/src/app/`

You can inspect your running container with  `exec -it yourcontainerid /bin/sh`, try browsing to /usr/src/app/data and note the readme.txt will appear once you done the copy above.

Sidenote: If you use a volume and do want to clear it, you'll need to remove your docker `docker rm yourcontainerid`, followed by docker `volume rm db`

Currently you need to visit it on your localhost (the api needs to be set for dynamic hostnames still) - soon!
