FROM node:10.13-alpine as build-stage
WORKDIR /app
COPY ./inventorious/search . 
COPY ./config .
# CMD npm install --prefix ./app/search --production
RUN npm install --production
RUN npm run-script build
# CMD npm start

FROM node:10.13-alpine

ARG HOST_ADDRESS=127.0.0.1
#ENV DOCKER_HOST $(ip -4 addr show wifi0 | grep -Po 'inet \K[\d.]+')
ENV DOCKER_HOST=$HOST_ADDRESS
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["inventorious-sqlite3-backend/package.json", "inventorious-sqlite3-backend/package-lock.json*", "inventorious-sqlite3-backend/npm-shrinkwrap.json*", "./"]
# RUN npm install --production
# RUN npm install --production --silent && mv node_modules ../
RUN set -x \
    && apk --no-cache --virtual build-dependencies add \
    python \
    nginx \
    make \
    g++ \
    && npm install \
    && apk del build-dependencies
COPY inventorious-sqlite3-backend/ .
RUN mkdir ./frontend
COPY --from=build-stage /app/build/ ./frontend

# COPY --from=build-stage /app/datafoo/ ./data
# COPY inventorious/search/build/ .frontend/
# COPY ../inventorious/search/build/ .
EXPOSE 4500
EXPOSE 3000
EXPOSE 80
CMD npm run-script startwithfrontend